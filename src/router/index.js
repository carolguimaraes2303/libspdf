import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/jspdfExemplo2',
    name: 'jspdfExemplo2',
    component: () => import('../components/jspdfExemplo2.vue')
  },
  {
    path: '/jspdfExemplo3',
    name: 'jspdfExemplo3',
    component: () => import('../components/jspdfExemplo3.vue')
  },
  {
    path: '/Exemplo2',
    name: 'Exemplo2',
    
    component: () => import('../components/pdfmake.vue')
  },
  {
    path: '/pdfmakeExemplo2',
    name: 'pdfmakeExemplo2',
    component: () => import('../components/pdfmakeExemplo2.vue')
  },
  {
    path: '/Exemplo3',
    name: 'Exemplo3',
    component: () => import('../components/htmlpdf.vue')
  },
  {
    path: '/htmlpdfExemplo2',
    name: 'htmlpdfExemplo2',
    component: () => import('../components/htmlpdfExemplo2.vue')
  },
  {
    path: '/Exemplo4',
    name: 'Exemplo4',
    component: () => import('../components/vue-pdf-app.vue')
  },
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
